local alpha = require("alpha")
local dashboard = require("alpha.themes.dashboard")

-- Set header
dashboard.section.header.opts.hl = ""
dashboard.section.header.val = {
	"  █▀█▒                 █▀█▒     ",
	" █  ██▒               █  ██▒    ",
	" █ ███▒               █ ███▒    ",
	"  ███▒  ▆   ▆          ███▒     ",
	"                            █▒  ",
	" ▄██████████████████▄        █▒ ",
	"         ▄▄▄▄▄    ▀███████▀  █▒ ",
	"                            █▒  ",
}

-- Footer
local function footer()
	-- Date
	local datetime = os.date "%a %d, %H:%M"

	-- Fetch $USER (UNIX)
	local handle = io.popen("whoami")
	local user = handle:read("*a")
	handle:close()
	local user = user:gsub("[ \n]", "")
	
	-- Change text depending on hour
	if(tonumber(os.date("%H")) > 12) then
		timeOfDay = "evening"
	else
		timeOfDay = "morning"
	end

	return string.format("Good %s %s. It's %s", timeOfDay, user, datetime)
end

dashboard.section.footer.opts.hl = "Comment"
dashboard.section.footer.val = footer()

-- Set buttons
local buttons = {
	dashboard.button("e", "> New file" , ":ene <BAR> startinsert <CR>"),
	dashboard.button("f", "> Find file", ":cd ./ | Telescope find_files<CR>"),
	dashboard.button("r", "> Recent"   , ":Telescope oldfiles<CR>"),
	dashboard.button("o", "> Orgfiles", ":cd ~/Nextcloud/Orgfiles | Telescope find_files<CR>"),
	dashboard.button("q", "> Quit Neobim", ":qa<CR>"),
}

-- Sets the width & cursor pos for every button
for i = 1, #buttons do
	buttons[i].opts.width = 30
	buttons[i].opts.cursor = 0
end

dashboard.section.buttons.val = buttons

-- Send config to alpha
alpha.setup(dashboard.opts)

-- Disable folding on alpha buffer
vim.cmd("autocmd FileType alpha setlocal nofoldenable")

