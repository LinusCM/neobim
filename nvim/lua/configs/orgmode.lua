-- Load custom tree-sitter grammar for org filetype
require('orgmode').setup_ts_grammar()

-- Tree-sitter configuration
require'nvim-treesitter.configs'.setup({
	highlight = {
		enable = true,
		additional_vim_regex_highlighting = {'org'},
	},
	ensure_installed = {'org'},
})

-- File location
require('orgmode').setup({
	org_agenda_files = {'~/Nextcloud/Orgfiles/**/*'},
	org_default_notes_file = '~/Nextcloud/Orgfiles/refile.org',
})

