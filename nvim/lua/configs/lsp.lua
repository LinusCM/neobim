local M = {}

local lsps = {
	"clangd",
	"omnisharp",
	"kotlin_language_server",
	"arduino_language_server",
	"jdtls",
	"gopls",
	"rust_analyzer",
	"rnix",

	"html",
	"ltex",
	"marksman",
	"jsonls",
	"yamlls",
	"taplo",
	"cssls",

	"tsserver",
	"pyright",
	"svelte",
	"vuels",
	"angularls",
	"bashls",
	"sumneko_lua",

	"grammarly"
}

function M.setup()
	for _, lsp in ipairs(lsps) do
		require("lspconfig")[lsp].setup({
			on_attach = M.on_attach,
			capabilities = M.capabilities,
		})
	end
end

return M

