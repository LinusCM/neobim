local map = vim.api.nvim_set_keymap
local default_opts = { noremap = true, silent = true }

-- Leader key
vim.g.mapleader = " "

-- Basic binds
-- Basic autopair
map('i', '"', '""<left>', default_opts)
map('i', '`', '``<left>', default_opts)
map('i', '(', '()<left>', default_opts)
map('i', '[', '[]<left>', default_opts)
map('i', '{', '{}<left>', default_opts)
map('i', '{<CR>', '{<CR}<ESC>0', default_opts)
map('i', '{;<CR>', '{<CR};<ESC>0', default_opts)

-- Move around splits using Ctrl + {n,e,o,i} (Workman layout)
map('n', '<C-n>', '<C-w>n', default_opts)
map('n', '<C-e>', '<C-w>e', default_opts)
map('n', '<C-o>', '<C-w>o', default_opts)
map('n', '<C-i>', '<C-w>i', default_opts)

-- Fast saving with <leader> and s
map('n', '<leader>s', ':w<CR>', default_opts)

-- Close all windows and exit from neovim
map('n', '<leader>qa', ':quitall<CR>', default_opts)

-- Clear search highlighting
map('n', '<leader>c', ':nohl<CR>', default_opts)

-- Convert spaces to tabs
map('n', '<leader>st', ':set noet|retab!<CR>', default_opts)

-- Lsp
map('n', '<leader>l', ':lua vim.lsp.buf.code_action()<CR>', default_opts)

-- Check diagnostics
map('n', '<leader>p', ':lua vim.diagnostic.goto_prev()<CR>', default_opts)
map('n', '<leader>n', ':lua vim.diagnostic.goto_next()<CR>', default_opts)

-- Telescope
map('n', '<leader>ff', ':cd ./ | Telescope find_files<CR>', default_opts)
map('n', '<leader>fb', ':Telescope buffers<CR>', default_opts)

-- Rest
map('n', '<leader>rr', '<Plug>RestNvim<CR>', default_opts)
map('n', '<leader>rp', '<Plug>RestNvimPreview<CR>', default_opts)
map('n', '<leader>rl', '<Plug>RestNvimLast<CR>', default_opts)

-- Neogit
map('n', '<leader>ng', ':Neogit<CR>', default_opts)

