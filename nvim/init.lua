-- Optimization
require("optimization/delay")

-- Options
require("options/packer")
require("options/plugins")
require("options/binds")
require("options/options")
require("options/colorscheme")

